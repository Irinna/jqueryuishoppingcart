-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-02-2019 a las 14:32:20
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `productos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prods`
--

CREATE TABLE `prods` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(30) NOT NULL,
  `precio` double NOT NULL,
  `nom` varchar(15) NOT NULL,
  `stock` int(5) NOT NULL,
  `foto` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `prods`
--

INSERT INTO `prods` (`id`, `descripcion`, `precio`, `nom`, `stock`, `foto`) VALUES
(1, 'Oli de Oliva Verge Extra', 6.8, 'Oli de Oliva', 65, 'aceite.jpg'),
(2, 'Ous de gallina Campera XL', 4.2, 'Ous XL', 80, 'huevos.png'),
(3, 'Harina de Trigo Gallo', 1.05, 'Harina', 289, 'harina.jpg'),
(4, 'Leche Puleva con calcio', 0.98, 'Leche', 197, 'leche.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `prods`
--
ALTER TABLE `prods`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `prods`
--
ALTER TABLE `prods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

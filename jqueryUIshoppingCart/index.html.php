<!DOCTYPE html>
<html>
<head>
	<title>JQuery UI Shopping Cart</title>
	<link rel="stylesheet" href="jquery-ui-1.12.1/jquery-ui.min.css">
	<script src="jquery-ui-1.12.1/external/jquery/jquery.js"></script>
	<script src="jquery-ui-1.12.1/jquery-ui.min.js"></script>
	<script src="jqueriui.js"></script>
</head>
<body>
	<h2>Llista de productes:</h2>
	<div id="dbitems">
		<?php
		include 'conect.php';
		$sql = 'select * from prods';
		$result = $conn -> query($sql);
		$cont=0;
		if ($result -> num_rows > 0) {
			while ($fila = $result -> fetch_assoc() ) {
				echo ('<div id="draggable'.$cont.'" class="ui-widget-content" value="'. $fila['id'] .'"><img title="'. $fila['descripcion'] .'"  id="'. $fila['id'] . '" width="150" height="150" src="resources/' . $fila['foto'] . '"><div id="nom">'. $fila['nom'] .'</div><div id="'. $fila['nom'] .'"></div><div id="'. $fila['precio'] .'">Preu:'.$fila['precio'].'€</div></div>');
				$cont++;
			}
		}
		?>
	</div>
	<div class="outer_container" style="overflow: scroll; overflow: hidden;width:550px; height: 280px; border: 2px solid black;">
		<div id="droppable" class="ui-widget-header"></div>
	</div>


	<button id="left"><<</button>
	<button id="right">>></button>
	<button id="empty">Reiniciar Carro</button>

	<script>
	$('#left').hover(function () {
		var leftPos = $('div.outer_container').scrollLeft();
		console.log(leftPos);
		$("div.outer_container").animate({
			scrollLeft: leftPos - 500
		}, 0);
	});

	$('#right').hover(function () {
		var leftPos = $('div.outer_container').scrollLeft();
		console.log(leftPos);
		$("div.outer_container").animate({
			scrollLeft: leftPos + 500
		}, 0);
	});

	$(".ui-widget-content").css("margin", "15px 15px 15px 15px");
	$(".ui-widget-content").css("height", "150px");
	$(".ui-widget-content").css("padding", "0.5em");
	$(".ui-widget-content").css("display", "inline-block");

	$("#dbitems").css("height", "250px");
	$("#dbitems").css("border", "2px dotted black");
	$("#dbitems").css("margin-bottom", "25px");

	$("#droppable").css("width", "780px");
	$("#droppable").css("height", "280px");
	$("#droppable").css("overflow-x", "scroll");
	$("#droppable").css("border", "2px solid black");
	$("#droppable").css("display", "flex");

	$(".outer_container").css("height", "260px");

</script>
</body>
</html>


var hoverProd;
var listaCompra = [];

$(function() {
  for(i = 0; i < $("#dbitems")[0].children.length; i++) {
    $("#draggable"+i).draggable();
    $('#empty').click(empty);
  }

  $("#droppable").droppable({
    drop: function( event, ui ) {
      var producte = $(ui.draggable.context);
      producte.css("left","0");
      producte.css("top","0");

      var cant = 0;
      var exist = false;

      if(listaCompra.length > 0) {
        var listaCarro = $("#droppable")[0].childNodes;
        var i;
        for (i = 0; i < listaCarro.length; i++) {
          if(listaCarro[i].id && listaCarro[i].id.substring("product-".length) == producte.context.id) {
            var itemCount = listaCarro[i].childNodes[2].textContent;
            if(itemCount != "") {
              cant=parseInt(itemCount);
            } else {
              cant=1;
            }
            exist = true;
            break;
          }
        }

        if(!exist) {
          addProduct(producte);
        } else{
          listaCarro[i].childNodes[2].innerText = (cant +1);
        }
      } else {
        addProduct(producte);
      }
    }
  });
});

function addProduct(producte) {
  listaCompra.push(producte);
  var newProduct = $(listaCompra[listaCompra.length -1]).clone();
  newProduct[0].id = "product-"+newProduct[0].id;

  var addbuttonId = 'addbutton-'+newProduct[0].id;
  var subsbuttonId = 'subsbutton-'+newProduct[0].id;
  var xbuttonId = 'xbutton-'+newProduct[0].id;

  var addbutton = $("<button id='"+addbuttonId+"'></button>").text("+");
  $(addbutton).click(increment);

  var subsbutton = $("<button id='"+subsbuttonId+"'></button>").text("-");
  $(subsbutton).click(decrement);

  var xbutton = $("<button id='"+xbuttonId+"'></button>").text("x");
  $(xbutton).click(deleteProd);

  newProduct.append(addbutton);
  newProduct.append(subsbutton);
  newProduct.append(xbutton);

  newProduct.appendTo($("#droppable"));
}

function empty() {
  listaCompra = [];
  $("#droppable").empty();
}

function increment(e) {
  var prodId = e.target.id.substring("addbutton-".length);
  var cantidad = parseInt($("#"+prodId)[0].childNodes[2].textContent);
  cantidad = cantidad ? cantidad + 1 : 2;
  $("#"+prodId)[0].childNodes[2].textContent = cantidad;
}

function decrement(e) {
  var prodId = e.target.id.substring("subsbutton-".length);
  var cantidad = parseInt($("#"+prodId)[0].childNodes[2].textContent) - 1;
  if(isNaN(cantidad) || cantidad <= 0){
    e.target.id = "xbutton-" + prodId;
    deleteProd(e);
  } else {
    $("#"+prodId)[0].childNodes[2].textContent = cantidad;
  }
}

function deleteProd(e) {
  var prodId = e.target.id.substring("xbutton-".length);
  $("#"+prodId).remove();
}
